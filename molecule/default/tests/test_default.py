import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_repo_file(host):
    f = host.file('/etc/yum.repos.d/wlcg.repo')

    assert f.exists
    assert f.user == 'root'
    assert f.group == 'root'


def test_yum_cache(host):
    vars = host.ansible.get_variables()
    print(vars)
    f = host.file('/var/cache/yum/x86_64/')
    assert f.exists
    assert f.is_directory
