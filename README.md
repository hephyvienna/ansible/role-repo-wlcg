Ansible Role repo-wlcg
======================

Install WLCG repository http://linuxsoft.cern.ch/wlcg/

Requirements
------------

* EL6/7 equivalent
* EPEL repository

Role Variables
--------------

     repo_wlcg_enabled: true
     repo_wlcg_priority: 90


Example Playbook
----------------

    - hosts: servers
      roles:
         - hephyvienna.repo-wcg

License
-------

MIT

Author Information
------------------

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in March 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
